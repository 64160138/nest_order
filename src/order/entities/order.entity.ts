Skip to content
GitLab
About GitLab
Pricing
Talk to an expert
Search GitLab
/
Help
Sign in
N
Nest_Order
Project information
Repository
Files
Commits
Branches
Tags
Contributors
Graph
Compare
Issues
0
Merge requests
0
CI/CD
Deployments
Packages and registries
Monitor
Analytics
Wiki
Snippets
Collapse sidebar
WP_141
Nest_Order
Repository

5 Order ./
Supanut Thamsaard authored 21 hours ago
2a3d79ec
nest_order
src
orders
entities
order.entity.ts
order.entity.ts
727 bytes
import { Customer } from 'src/customers/entities/customer.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { OrderItem } from './order-item';
@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  amount: number;
  @Column({ type: 'float' })
  total: number;
  @ManyToOne(() => Customer, (customer) => customer.order)
  customer: Customer;
  @CreateDateColumn()
  createdData: Date;
  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;
  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];
}
