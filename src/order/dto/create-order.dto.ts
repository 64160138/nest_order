import { IsNotEmpty, IsPositive } from 'class-validator';
class CreatedOrderItemDto {
  @IsNotEmpty()
  @IsPositive()
  productId: number;

  @IsNotEmpty()
  @IsPositive()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  @IsPositive()
  customerId: number;
  orderItems: CreatedOrderItemDto[];
}
