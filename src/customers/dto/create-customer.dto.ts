import { IsNotEmpty, Length, IsPositive } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(2, 1000)
  name: string;
  @IsPositive()
  @IsNotEmpty()
  age: number;
  @IsNotEmpty()
  tel: string;
  @IsNotEmpty()
  @Length(1, 6)
  gender: string;
}
